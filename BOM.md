
	- This is the list of materials to build an auto-tracking satellite antenna 
		○ Solar-powered. Fully autonomous (no cables)
		○ Can be put anywhere in WiFi range
	- NOTE
		○ List not final! 
		○ Prices on Amazon Prime only
			§ Not the cheapest but fastest delivery
			§ You are encouraged to look for alternate prices - you may get much better deals somewhere else
	- Power
		○ Solar panel 
			§ Requirements: 
				□ waterproof
				□ Needs enough power to run RqasPi 24/7 and the motors when needed 
				□ NOTE: still needs to be validated! (we may need a more powerful one?)
			§ $24.99 
				□ https://www.amazon.com/gp/product/B07V7RJYGY/ref=ppx_yo_dt_b_asin_title_o05_s00?ie=UTF8&psc=1
		○ Pass-through power bank 
			§ Requirements
				□ pass-through (charge and discharge at the same time! RAVpower ones meet this criteria in general)
				□ RasPi current draw must not drop when the servos are activated
				□ Must work at sufficiently low temperature
				□ Must power the RasPi constantly 24/7 
				□ NOTE: still needs to be validated! (we may need a more powerful one?)
			§ $13.99 
				□ https://www.amazon.com/gp/product/B07VL4H3Y5/ref=ppx_yo_dt_b_asin_title_o06_s00?ie=UTF8&psc=1 
	- Electronics
		○ 3D Accelerometer/magnetometer/compass/gyro MPU9250 
			§ Needs to have compass (magnetomoeter) and gyro/inclinometer 
			§ https://www.amazon.com/HiLetgo-Gyroscope-Acceleration-Accelerator-Magnetometer/dp/B01I1J0Z7Y
		○ Raspberry Pi
			§ 3B/3B+/4 
				□ Cam $35 ? 
		○ MicroSD card
			§ 4 GB or more needed 
			§ Not a large one but a good quality one recommended (Sandisk/Samsung)
		○ Servo control board
			§ $6.99 
				□ https://www.amazon.com/gp/product/B01D1D0CX2 
	- RF
		○ (recommended!) FUNcube dongle
			§ $180? 
				□ http://www.funcubedongle.com/ 
			§ FlightAware SDR is a good alternative though (much cheaper)
		○ FlightAware RTL-SDR
			§ (sau un alt RTL-SDR cu built-in LNA, low-noise floor < 1 dB, preferabil 0.6 dB max)
			§ $18.75
				□ https://www.amazon.com/FlightAware-Pro-Stick-ADS-B-Receiver/dp/B01D1ZAP3C 
		○ (optional?) standalone LNA
			§ For an existing RTL-SDR without LNA - needs to have bias-T power though  
			§ $18.95
				□ https://www.amazon.com/gp/product/B07G14Q6XX/ref=ppx_yo_dt_b_asin_title_o02_s00?ie=UTF8&psc=1 
			§ $29.95
				□ https://www.amazon.com/gp/product/B07XNLJ9X2/ref=ppx_yo_dt_b_asin_title_o02_s00?ie=UTF8&psc=1 
		○ 433 MHz Dipole 
			§ $6.49 
				□ https://www.amazon.com/gp/product/B07DMXSX45/ref=ppx_yo_dt_b_asin_title_o02_s02?ie=UTF8&psc=1 
			§ Need two for cross-Yagi
		○ Clip-on Ferrites
			§ $10.99 
				□ https://www.amazon.com/gp/product/B07DPM44BV/ref=ppx_yo_dt_b_asin_title_o03_s01?ie=UTF8&psc=1 
	- Mechanical
		○ Servos
			§ $21.99 
				□ https://www.amazon.com/gp/product/B07CMBMWZW 
			§ Need two per antenna
			§ NOTE: still needs to be validated! 
		○ Waterproof enclosure
			§ $19.99
				□ https://www.amazon.com/gp/product/B07YBXSG1J/ref=ppx_yo_dt_b_asin_title_o09_s00?ie=UTF8&psc=1 
			§ Note: smaller one may also work? 
		○ Cable glands
			§ $11.69
				□ https://www.amazon.com/gp/product/B077D8MP53/ref=ppx_yo_dt_b_asin_title_o01_s00?ie=UTF8&psc=1
			§ Needed to allow cables to protrude the waterproof box
		○ 1515 MakerBeamXL - main T-slot beams
			§ $43 (1000 mm x4)
				□ https://www.amazon.com/gp/product/B06XJ42W51/ref=ppx_yo_dt_b_asin_title_o02_s00?ie=UTF8&psc=1
			§ Need two per antenna
			§ Already bought enough 
		○ 3/16 Aluminum tubing 
			§ $31.74 (3' x 6) 
				§ https://www.amazon.com/gp/product/B0006N6J7O/ref=ppx_yo_dt_b_asin_title_o02_s01?ie=UTF8&psc=1 
		○ 3/4" Square tube (3 ft)
			§ For the counterbalance extension in the back
			§ $15.47
				§ https://www.amazon.com/Forney-49250-Square-Aluminum-Tubing/dp/B003X3UEA2/ref=sr_1_3?keywords=aluminum+square+tube&qid=1581982940&sr=8-3 
		○ M3 screws set
			§ $19.99 
				§ https://www.amazon.com/gp/product/B07VNDFYNQ/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1 
	- Cables
		○ (optional?) USB extension cable
			§ $3.45 
				□ https://www.amazon.com/gp/product/B0044AEYF4/ref=ppx_yo_dt_b_asin_title_o08_s00?ie=UTF8&psc=1 
			§ Needed to attach the RTL-SDR or FunCube to RasPi
			§ Needed so the assembly can fit in the box! 
		○ Power cable for RasPi
			§ $7.99 (x2)
				□ https://www.amazon.com/gp/product/B00S8GU03A/ref=ppx_yo_dt_b_asin_title_o08_s01?ie=UTF8&psc=1 
			§ Needed to connect the RasPi to power bank
			§ Need right-angle so RasPi can fit in the box! 
		○ Circuit Board jumper cables 
			§ $6.98
				□ https://www.amazon.com/Elegoo-EL-CP-004-Multicolored-Breadboard-arduino/dp/B01EV70C78 
			§ Needed to connect various components 
			§ MM/MF/FF set
			§ Get the high-quality one with square-shape connectors (not the round-shape ultra-cheap breadboard cables)
