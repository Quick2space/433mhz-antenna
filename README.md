# README #

Description of antenna setup for receiving HuskySat-1 data

### What is this repository for? ###

* Quick summary: initial 433 MHz antenna design and associated system integration to get telemetry/camera data from HuskySat-1
* Version: 0.1 

### Overview ###

The system consists in an antenna hooked up to a laptop. The antenna can be manually pointed by the user toward the satellites passing by. 

### Antenna design ###

* Description
	* Largely follows the 6-element U6YRN antenna design. 
	* All elements are under 35.2 cm (just under 14")
	* We use this design "as is" to work with a pre-made impedance-matched dipole (see below)
	* Initial design: http://www.iw5edi.com/technical-articles/6-element-for-432-mhz 
	* (TODO: add 3D model)
	* Goal: Solid design, beginner-friendly. No need to worry about poor connectors. 

### Materials ###

* 3D printed mounting brackets for elements/reflector/dipole
	* 3D printed parts - see associated Thingiverse project - https://www.thingiverse.com/thing:4103859/
	* Note: this 3D design is parametric! Customizer link: https://www.thingiverse.com/apps/customizer/run?thing_id=4103859
* Boom mount 
	* MakerBeam XL 1515 T-slot
	* https://www.amazon.com/gp/product/B06XJ42W51 
* Dipole 
	* We went with pre-made 433 MHz antenna dipole 
	* https://www.amazon.com/Zerone-433MHZ-Antenna-Signal-Amplifier/dp/B07DMXSX45/ 
	* Advantage = pre-tuned, already coupled with a coax cable and an SMA connector
* Directors/reflector
	* 14" brass rods (3/16) 
	* https://www.amazon.com/gp/product/B07MGVKCBW 
	* Need to be cut to length
* Screws 
	* The design uses 8 mm or 10 mm M3 screws. * M3 was chosen because M3 nuts are fitting nicely in the MakerBeam XL beam without extra adapters required)
	* Example of a screw set here: https://www.amazon.com/gp/product/B07VNDFYNQ/  
* RTL-SDR 
	* We used the FlightAware SDR - built-in LNA! 
	* Works with the dipole coax above
	* https://www.amazon.com/gp/product/B01D1ZAP3C 

### Software ###

* To get data - use FoxTelem 1.08 or newer
	* Updated by Chris,  AC2CZ to be compatible with HuskySat-1
* Camera packet on-wire format:
	* https://bitbucket.org/Quick2space/mcp25625.py/src/master/docs/SSF-v03.pdf 
* To render camera packets - Python script for parsing and rendering HuskySat-1 camera packets
	*  https://bitbucket.org/Quick2space/mcp25625.py/src/master/scripts/prc3.py 
* Hook-up code 
	* TBD 
* Tracking satellites software
	* SatPC32 - http://www.dk1tb.de/indexeng.htm 


### Acknowledgements ###

* Volunteers: 
	* Corina Ciustea, Skyline HS, students project coordinator
	* Marius Gheorghescu - 3D printing models
	* Adi Oltean, K7ADI - antenna design, interfacing, camera board
	* John Petrich, W7FU - antenna design, RTL-SDR
	* Mariana Varrotto, WA7EE - RTL-SDR, antenna design
	

### Who do I talk to? ###

* Repo owner or admin: adi.oltean@quick2space.org
