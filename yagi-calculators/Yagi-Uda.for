C      PROGRAM YAGI_UDA.F    
C     ******************************************************************
C     THIS IS A FORTRAN PROGRAM THAT COMPUTES, FOR THE YAGI-UDA ARRAY, 
C     THE 
C     
C       I.    FAR-ZONE E- AND H-PLANE AMPLITUDE PATTERNS (IN dB)
C       II.   DIRECTIVITY OF THE ARRAY (IN dB)
C       III.  E-PLANE HALF-POWER BEAMWIDTH (IN DEGREES)
C       IV.   H-PLANE HALF-POWER BEAMWIDTH (IN DEGREES)
C       V.    E-PLANE FRONT-TO-BACK RATIO (IN dB)
C       VI.   H-PLANE FRONT-TO-BACK RATIO (IN dB)
C
C     THE PROGRAM IS BASED ON POCKLINGTON'S INTEGRAL EQUATION 
C     FORMULATION OF SECTION 10.3.3, EQUATIONS (10-42) - (10-65a).  
C     M ENTIRE DOMAIN COSINUSOIDAL (FOURIER) BASIS MODES ARE USED 
C     FOR EACH OF THE ANTENNA ELEMENTS.
C
C       ** INPUT PARAMETERS
C       1. M     =   NUMBER OF ENTIRE DOMAIN BASIS MODES
C       2. N     =   NUMBER OF ANTENNA ELEMENTS
C       3. L     =   LENGTH OF EACH ELEMENT (IN WAVELENGTHS)
C       4. ALPHA =   RADIUS OF EACH ELEMENT (IN WAVELENGTHS)
C       5. S     =   SEPARATION BERWEEN THE ELEMENTS (IN WAVELENGTHS)
C
C       ** NOTES
C       1.  REFER TO FIGURE 10.17 FOR THE GEOMETRY. 
C       2.  DRIVER ELEMENT IS LOCATED AT THE ORIGIN.
C       3.  FIRST ELEMENT (N=1) IS THE FIRST DIRECTOR.
C       4.  REFLECTOR IS THE N-1 ELEMENT; ONLY ONE REFLECTOR. 
C       5.  DRIVEN ELEMENT IS N.
C
C     THE FORMULATION OF THE PROBLEM IS BASED ON THE PAPER `ANALYSIS OF
C     YAGI-UDA-TYPE ANTENNAS' BY GARY A. THIELE, IEEE TRANS. ANTENNAS
C     PROPAGAT., VOL. 17, JAN. 1969.  
C     ******************************************************************
C     Written by: Panayiotis A. Tirkas, Arizona State University
C
C     ******************************************************************

      PARAMETER (MMAX=30,NMAX=30)
C
      EXTERNAL FF,FXZ,PHASE,ZMINUS,ZPLUS
      COMPLEX A(MMAX*NMAX,MMAX*NMAX),Inm(NMAX,MMAX),B(MMAX*NMAX)
      COMPLEX CJ,RES,G2,IZP,EZP,AZ,PRAD,ETHETA(361)
C
      REAL Z,ETA,PI,L(NMAX),DZ(NMAX),K,UL,LL,F2M,ALPHA,ETH(361)
      REAL RHO,RTOD,S(NMAX-1),YP(NMAX),CUR(101),PHA(101)
      REAL MU,THETA,PHI,DTOR,ARG,C,EMAX,LEN,ANG,PIVOT(MMAX*NMAX)
      REAL ZMINUS,ZPLUS,AEXP,LDIR,SDIR
      INTEGER I,J,INDEX,NMODE,DEVICE,IPERM(MMAX*NMAX)
      CHARACTER*12 FILNAM
      CHARACTER*1 ANS
C
      COMMON/EXTRAS/Z,RHO,N2,NMODE

C     OPEN OUTPUT FILES 
C     -----------------
      OPEN(UNIT=1,FILE='Epl-yagi.dat',STATUS='UNKNOWN')
      OPEN(UNIT=2,FILE='Hpl-yagi.dat',STATUS='UNKNOWN')
      OPEN(UNIT=3,FILE='Cur-yagi.dat',STATUS='UNKNOWN')
      OPEN(UNIT=4,FILE='Coe-yagi.dat',STATUS='UNKNOWN')
C

C     ********** CHOICE OF OUTPUT **********
C
      WRITE(6,610)
      READ(5,620,ERR=999) DEVICE

      IF(DEVICE .EQ. 1) THEN
         DEVICE=6
      ELSE IF(DEVICE .EQ. 2) THEN
         DEVICE=7
         WRITE(6,630)
         READ(5,*,ERR=999) FILNAM
         OPEN(UNIT=DEVICE,FILE=FILNAM,STATUS='UNKNOWN')
      ELSE 
         WRITE(6,640)
         STOP
      ENDIF

C
C INPUT THE LENGTH OF THE ELEMENTS, L, AND THEIR RELATIVE SEPARATION IN FREE
C SPACE WAVELENGTHS, S.  THE VARIABLE YP DEFINES THE ABSOLUTE DISTANCE OF
C THE ELEMENTS ALONG THE Y-AXIS, WITH THE DRIVEN ELEMENT AT THE ORIGIN.      
C ALPHA IS THE ELEMENT WIRE RADIUS IN WAVELENTGHS.
C
C     ***********************************************************************

C     INPUT NUMBER OF MODES PER ELEMENT
C     ---------------------------------
      WRITE(6,720)
      READ(5,*,ERR=999) M
      IF(M .GT. MMAX) THEN
         WRITE(6,603) 
         STOP
      ENDIF

C     INPUT NUMBER OF ELEMENTS
C     ------------------------
      WRITE(6,730)
      READ(5,*,ERR=999) N
      IF(N .GT. NMAX) THEN
         WRITE(6,604)
         STOP
      ENDIF


C     INPUT ELEMENT LENGTHS AND ELEMENT SEPARATION DISTANCES
C     ------------------------------------------------------
C     ---> ELEMENT LENGTHS (IN WAVELENGTHS)
      IF(N .GT. 3) THEN
         WRITE(6,735)
         READ(5,*,ERR=999) ANS
      ELSE
         ANS='N'
      ENDIF
      IF(ANS .EQ. 'Y' .OR. ANS .EQ. 'y') THEN
         WRITE(6,736)
         READ(5,*,ERR=999) LDIR
         DO I=1,N-2
            L(I)=LDIR
         ENDDO
      ELSE
         DO I=1,N-2
            WRITE(6,740) I
            READ(5,*,ERR=999) L(I)
         ENDDO
      ENDIF
      WRITE(6,742)
      READ(5,*,ERR=999) L(N-1)
      WRITE(6,744)
      READ(5,*,ERR=999) L(N)

C     ---> ELEMENT SEPARATION (IN WAVELENGTHS)
      WRITE(6,754)
      READ(5,*,ERR=999) S(1)

      IF(N .GT. 3) THEN
         WRITE(6,737)
         READ(5,*,ERR=999) ANS
      ELSE
         ANS='N'
      ENDIF
      IF(ANS .EQ. 'Y' .OR. ANS .EQ. 'y') THEN
         WRITE(6,738)
         READ(5,*,ERR=999) SDIR
         DO I=2,N-2
            S(I)=SDIR
         ENDDO
      ELSE
         DO I=2,N-2
            WRITE(6,750) I-1,I         
            READ(5,*,ERR=999) S(I)
         ENDDO
      ENDIF
      WRITE(6,752)
      READ(5,*,ERR=999) S(N-1)

      WRITE(6,760)   
      READ(5,*,ERR=999) ALPHA

      DO I=1,N-2
         YP(I)=I*S(I)
      ENDDO
      YP(N-1)=-S(N-1)
      YP(N)=0.0

C     *********************************************************************

C     ECHO INPUT PARAMETERS
C     ---------------------

      WRITE(DEVICE,800)
      WRITE(DEVICE,820) M
      WRITE(DEVICE,830) N

C     ---> ELEMENT LENGTHS (IN WAVELENGTHS)
      DO I=1,N-2
         WRITE(DEVICE,840) I,L(I)
      ENDDO
      WRITE(DEVICE,842) L(N-1)
      WRITE(DEVICE,844) L(N)

C     ---> ELEMENT SEPARATION (IN WAVELENGTHS)
      WRITE(DEVICE,854) S(1)
      DO I=2,N-2
         WRITE(DEVICE,850) I-1,I,S(I)       
      ENDDO
      WRITE(DEVICE,852) S(N-1)

      WRITE(DEVICE,860) ALPHA   

      WRITE(6,770)
C     ********************************************************************
C 
      DO 5 I=1,N
         DZ(I)=L(I)/FLOAT(2*M-1)
 5    CONTINUE
C
C     INITIALIZE SOME VARIABLES AND ARRAYS
C
      CJ=CMPLX(0.0,1.0)
      RES=CMPLX(0.0,0.0)
      G2=CMPLX(0.0,0.0)
      INDEX=0
      PI=4.0*ATAN(1.0)
      RTOD=180.0/PI
      DTOR=PI/180.0
      ETA=120.0*PI
      MU=4.0*PI*1.0E-7
      C=3.0*1.0E+8
      K=2.0*PI
C
      DO 15 I=1,M*N
         B(I)=CMPLX(0.0,0.0)
         DO 15 J=1,M*N
            A(I,J)=CMPLX(0.0,0.0)
 15   CONTINUE
C
      DO 25 I=1,N
         DO 25 J=1,M
            Inm(I,J)=CMPLX(0.0,0.0)
   25 CONTINUE
C
C FILL IN THE MATRICES A, B OF THE SYSTEM A*X=B
C
      DO 10 I=1,M*N
C
C THIS DETERMINES THE POSITION OF THE OBSERVER WITH
C N1 BEING THE ELEMENT AT WHICH THE OBSERVER IS
C
         IFACT=(I-1)/M
         N1=IFACT+1
         IMODE=I-IFACT*M
C BASED ON THE MODE # AND ELEMENT FIND THE DISTANCE Z
         Z=FLOAT(M-IMODE)*DZ(N1)
C
         DO 20 J=1,M*N
C
C THIS DETERMINES THE POSITION OF THE SOURCE AND ITS CORRESPONDING MODE.
C N2 IS THE ELEMENT ON WHICH THE SOURCE IS AND NMODE ITS MODE NUMBER.
C
            JFACT=(J-1)/M
            N2=JFACT+1
            NMODE=J-JFACT*M
C
C IF THE EFFECT OF A MODE IS FOUND ON THE ELEMENT THAT IT IS
C LOCATED THEN Y IS THE RADIUS OF THE ELEMENT, OTHERWISE THE DISTANCE
C Y IS FOUND USING THE FORMULA (ASSUME X, X' = 0.0).
C
            IF(N1.EQ.N2)THEN
               RHO=ALPHA
            ELSE
               RHO=YP(N1)-YP(N2)
            ENDIF
C
C DEFINE THE LIMITS OF INTEGRATION
C
            LL=0.0
            UL=L(N2)/2.0
C
C PERFORM NUMERICAL INTEGRATION OF THE KERNEL
C
            CALL SINTEG(FF,UL,LL,10,RES,L)
            LEN=L(N2)/2.0
            CALL KERNEL(G2,LEN)
            F2M=FLOAT(2*NMODE-1)
            A(I,J)=ETA/(8.0*PI*PI*CJ)*((F2M*PI/L(N2))*(-1.0)**
     &            (NMODE+1)*G2+(K*K-F2M*F2M*PI*PI/L(N2)/L(N2))*RES)
 20      CONTINUE
 10   CONTINUE
C
C FILL THE LAST ROW OF THE MATRIX CORRESPONDING TO THE FEEDER.
C
      I=M*N
      DO 30 J=1,M*N
         IF(J.GT.(M*(N-1)))THEN
            A(I,J)=CMPLX(1.0,0.0)
         ELSE
            A(I,J)=CMPLX(0.0,0.0)
         ENDIF
 30   CONTINUE
      B(M*N)=CMPLX(1.0,0.0)
C
C INVERT THE SYSTEM TO SOLVE FOR THE CURRENT COEFFICIENTS IN THE
C FOURIER SERIES EXPANSION.   
C
      ISIZE=N*M
      CALL LUDEC(A,ISIZE,NMAX*MMAX,IPERM,PIVOT)
      CALL LUSOLV(A,ISIZE,NMAX*MMAX,IPERM,B)

C
C CONVERT THE SINGLE ARRAY OF THE CURRENT COEFFICIENTS TO A
C DOUBLE ARRAY OF THE FORM Imn.
C
      NCUT=0
      DO 60 I=1,N
         DO 70 J=1,M
            Inm(I,J)=B(J+NCUT)
 70      CONTINUE
         NCUT=NCUT+M
 60   CONTINUE
C
C CALCULATE THE RADIATED FIELDS IN THE E-PLANE
C
C IN THIS PLANE THETA VARIES FROM 0 < THETA < 180, WHEREAS
C PHI = 90 IN HALF OF THE PATTERN AND PHI=270 IN THE OTHER 
C HALF.  THE PATTERN IS CALCULATED AT ONE DEGREE INCREMENTS. 
C 
      NCUT=0
      DO 125 ML=1,2
         IF(ML.EQ.1)THEN
            PHI=90.0*DTOR
            MAX=181
         ELSE
            PHI=270.0*DTOR
            MAX=180
         ENDIF
C
         DO 130 ICOUNT=1,MAX
            THETA=FLOAT(ICOUNT-1)*DTOR
            IF(THETA.GT.PI)PHI=270.0*DTOR
            EZP=CMPLX(0.0,0.0)
            DO 140 I=1,N
               IZP=CMPLX(0.0,0.0)
               DO 150 J=1,M
                  MODE=J
                  LEN=L(I)
                  ANG=THETA
                  IZP=IZP+Inm(I,J)*(ZMINUS(ANG,LEN,MODE)+ZPLUS(ANG,LEN
     &                  ,MODE))
 150           CONTINUE
               AEXP=K*YP(I)*SIN(THETA)*SIN(PHI)
               EZP=EZP+L(I)*CEXP(CJ*AEXP)*IZP
 140        CONTINUE
            ETHETA(NCUT+ICOUNT)=CJ*C*MU/8.0*SIN(THETA)*EZP
 130     CONTINUE
         NCUT=NCUT+MAX
 125  CONTINUE
C
C  FIND THE MAXIMUM VALUE IN THE E-PLANE PATTERN
C
      EMAX=1.0*1E-12

      DO 160 I=1,361
         ARG=CABS(ETHETA(I))
         IF(ARG.GT.EMAX)EMAX=ARG
 160  CONTINUE
C
C NORMALIZE THE PATTERN TO THE MAXIMUM VALUE, CONVERT IN dB, 
C AND WRITE OUT THE RESULTS
C
      WRITE(1,606)
      DO 170 I=1,361
         THETA=FLOAT(I-1)
         ARG=CABS(ETHETA(I))
         IF((ARG/EMAX).GT.(1.0*1E-6))THEN
            ETH(I)=20*ALOG10(ARG/EMAX)
         ELSE
            ETH(I)=-120.0
         ENDIF
         WRITE(1,300)THETA,ETH(I)
 170  CONTINUE
 300  FORMAT(1X,2F12.4)
C
C FIND THE FRONT-TO-BACK RATIO IN THE E-PLANE PATTERN 
C
      EFTOB=-ETH(271)

C
C FIND THE 3-dB BEAMWIDTH IN THE E-PLANE PATTERN
C
      DO 173 I=91,270
         ETH(I)=ETH(I) + 3.0
 173  CONTINUE

      DO 175 I=91,270
         THETA=FLOAT(I-1)
         IF(ETH(I).EQ.0.0)THEN
            E3D_BW=2.0*(FLOAT(I-1)-90.0)
            GOTO 178
         ELSE IF(ETH(I-1).GT.0.0.AND.ETH(I).LT.0.0)THEN
            E3D_BW=2.0*(-ETH(I)/(ETH(I)-ETH(I-1))+FLOAT(I-1)-90.0)
            GOTO 178
         ENDIF
 175  CONTINUE
 178  CONTINUE
        
C
C CALCULATE THE RADIATED FIELDS IN THE H-PLANE 
C
C IN THIS PLANE THETA = 90 AND PHI VARIES FROM 0 < PHI < 360.
C
      THETA=90.0*DTOR
      MAX=361
C
      DO 180 ICOUNT=1,MAX
         PHI=FLOAT(ICOUNT-1)*DTOR
         EZP=CMPLX(0.0,0.0)
         DO 190 I=1,N
            IZP=CMPLX(0.0,0.0)
            DO 200 J=1,M
               MODE=J
               LEN=L(I)
               ANG=PHI
               IZP=IZP+Inm(I,J)*(ZMINUS(ANG,LEN,MODE)+ZPLUS(ANG,LEN,MODE
     &               ))
 200        CONTINUE
            AEXP=K*YP(I)*SIN(THETA)*SIN(PHI)
            EZP=EZP+L(I)*CEXP(CJ*AEXP)*IZP
 190     CONTINUE
         ETHETA(ICOUNT)=CJ*C*MU/8.0*SIN(THETA)*EZP
 180  CONTINUE
C
C  FIND THE MAXIMUM VALUE IN THE H-PLANE PATTERN
C
      EMAX=1.0*1E-12

      DO 210 I=1,361
         ARG=CABS(ETHETA(I))
         IF(ARG.GT.EMAX)EMAX=ARG
 210  CONTINUE
C
C NORMALIZE THE PATTERN TO THE MAXIMUM VALUE, CONVERT IN dB, AND 
C WRITE OUT THE RESULTS
C
      WRITE(2,607)
      DO 220 I=1,361
         PHI=FLOAT(I-1)
         ARG=CABS(ETHETA(I))
         IF((ARG/EMAX).GT.(1.0*1E-6))THEN
            ETH(I)=20*ALOG10(ARG/EMAX)
         ELSE
            ETH(I)=-120.0
         ENDIF
         WRITE(2,300)PHI,ETH(I)
 220  CONTINUE
C
C FIND THE FRONT-TO-BACK RATIO IN THE H-PLANE PATTERN 
C
      HFTOB=-ETH(271)
C
C FIND THE 3-dB BEAMWIDTH IN THE H-PLANE PATTERN
C
      DO 223 I=1,181
         ETH(I)=ETH(I) + 3.0
 223  CONTINUE

      DO 225 I=91,270
         PHI=FLOAT(I-1)
         IF(ETH(I).EQ.0.0)THEN
            H3D_BW=2.0*(FLOAT(I-1)-90.0)
            GOTO 228 
         ELSE IF(ETH(I-1).GT.0.0.AND.ETH(I).LT.0.0)THEN
            H3D_BW=2.0*(-ETH(I)/(ETH(I)-ETH(I-1))+FLOAT(I-1)-90.0)
            GOTO 228
         ENDIF
 225  CONTINUE
 228  CONTINUE

C
C CALCULATE THE ANTENNA DIRECTIVITY
C
      THETA=90.0*DTOR
      PHI=90.0*DTOR
      AZ=CMPLX(0.0,0.0)
      DO 230 I=1,N
         IZP=CMPLX(0.0,0.0)
         DO 240 J=1,M
            MODE=J
            LEN=L(I)
            ANG=THETA
            IZP=IZP+Inm(I,J)*(ZMINUS(ANG,LEN,MODE)+ZPLUS(ANG,LEN,MODE))
 240     CONTINUE
         AEXP=K*YP(I)*SIN(THETA)*SIN(PHI)
         AZ=AZ+L(I)*CEXP(CJ*AEXP)*IZP
 230  CONTINUE
      UMAX=3.75*PI*CABS(AZ)*CABS(AZ)*SIN(THETA)*SIN(THETA)
C
      CALL SCINT2(FXZ,0.0,PI,0.0,2.0*PI,PRAD,N,M,Inm,L,YP,NMAX,MMAX)
C
      D0=4.0*PI*UMAX/CABS(PRAD)
C
      WRITE(DEVICE,650)
      WRITE(DEVICE,660) E3D_BW
      WRITE(DEVICE,670) H3D_BW
      WRITE(DEVICE,680) EFTOB
      WRITE(DEVICE,690) HFTOB
C
      WRITE(DEVICE,700) 10*ALOG10(D0)
C
C     BASED ON THE FOURIER COEFFICIENTS OF THE CURRENT, CALCULATE THE 
C     CURRENT DISTRIBUTION ON THE ELEMENTS.  NOTE THAT EACH ELEMENT 
C     IS SUBDIVIDED INTO 100 SECTIONS FOR THIS CALCULATION.
C
C
      WRITE(3,710)
      DO 245 IL=1,N
         WRITE(3,400) IL
         DZ(IL)=L(IL)/100.0
         DO 250 I=1,51
            Z=FLOAT(I-1)*DZ(IL)
            IZP=CMPLX(0.0,0.0)
            DO 260 J=1,M
               F2M=FLOAT(2*J-1)
               IZP=IZP+Inm(IL,J)*COS(F2M*PI*Z/L(IL))
 260        CONTINUE
            CUR(I)=CABS(IZP)
            PHA(I)=PHASE(IZP)*RTOD
 250     CONTINUE
C
C
         WRITE(3,450)
         DO 270 I=1,51
            Z=FLOAT(I-1)*DZ(IL)
            WRITE(3,500) Z,CUR(I),PHA(I)
 270     CONTINUE
C

 245  CONTINUE
C
C
      DO 280 I=1,N
         WRITE(4,400) I
         WRITE(4,550)
         DO 290 J=1,M
            CURRENT=CABS(Inm(I,J))
            ANGLE=PHASE(Inm(I,J))*RTOD
            WRITE(4,600) J,CURRENT,ANGLE
 290     CONTINUE
 280  CONTINUE


      IF(DEVICE .EQ. 6) THEN
         WRITE(DEVICE,605)
      ELSE IF(DEVICE .EQ. 7) THEN
         WRITE(6,605)
         WRITE(DEVICE,605)
      ENDIF


C     **** FORMAT STATEMENTS **** 
C

 400  FORMAT(3X,'ELEMENT # ',I3,/,3X,'=========',/)
 450  FORMAT(7X,'DISTANCE',7X,' CURRENT ',4X,'CURRENT',/,
     $      7X,'        ',7X,'MAGNITUDE',4X,' PHASE ',/)
 500  FORMAT(3X,F12.5,3X,F12.5,3X,F12.5)
 550  FORMAT(2X,'MODE # ',3X,'MAGNITUDE',6X,'PHASE',/)
 600  FORMAT(3X,I2,3X,F12.5,3X,F12.5)
 603  FORMAT(3X,'*** ERROR: Need to increase MMAX in the program.',/)
 604  FORMAT(3X,'*** ERROR: Need to increase NMAX in the program.',/)
 605  FORMAT(//,3X,'*** NOTE:',/,
     $ 7X,'E-PLANE PATTERN IS STORED IN Epl-yagi.dat',/,
     $ 7X,'H-PLANE PATTERN IS STORED IN Hpl-yagi.dat',/,
     $ 7X,'CURRENT ON EACH ELEMENT IS STORED IN Cur-yagi.dat',/,
     $ 7X,'MODE COEFFs. FOR EACH ELEMENT ARE STORED IN Coe-yagi.dat',
     $ /)

 606  FORMAT('# E-PLANE PATTERN OF THE YAGI UDA ANTENNA',/,
     $      '# =======================================',/,'#',/,
     $      '#      THETA      E-THETA (THETA, PHI=90 OR 270)',/,'#')
 607  FORMAT('# H-PLANE PATTERN OF THE YAGI UDA ANTENNA',/,
     $      '# =======================================',/,'#',/,
     $      '#       PHI        E-THETA (PHI, THETA=90)',/,'#')

 610  FORMAT(3X,'OUTPUT DEVICE OPTION',/,6X
     $      ,'OPTION (1): SCREEN',/,6X,'OPTION (2): OUTPUT FILE',//,3X
     $      ,'OUTPUT DEVICE = ',$)

 620  FORMAT(I1)

 630  FORMAT(3X,'INPUT THE DESIRED OUTPUT FILNAME (in single quotes) = '
     &      ,$)

 640  FORMAT(/,3X,'*** ERROR ***',/,3X
     $      ,'OUTPUTING DEVICE NUMBER SHOULD BE EITHER 1 OR 2')

 650  FORMAT(/,3X
     &      ,'******************************************************',/
     &      ,3X,'PROGRAM OUTPUT FOR THE YAGI UDA ARRAY',/,3X
     &      ,'******************************************************',/)

 660  FORMAT(/,3X,'3-dB BEAMWIDTH IN THE E-PLANE PATTERN = ',
     &      F12.2,2X,'DEGREES')
 670  FORMAT(/,3X,'3-dB BEAMWIDTH IN THE H-PLANE PATTERN = ',
     &      F12.2,2X,'DEGREES') 
 680  FORMAT(/,3X,'FRONT-TO-BACK RATIO IN THE E-PLANE = ',F12.4,2X,'dB')
 690  FORMAT(/,3X,'FRONT-TO-BACK RATIO IN THE H-PLANE = ',F12.4,2X,'dB')
 700  FORMAT(/,3X,'DIRECTIVITY = ',F16.3,2X,'dB')
 710  FORMAT(3X,'CURRENT DISTRIBUTION ON THE ELEMENTS',/,3X
     &      ,'====================================',/)
 720  FORMAT(/,3X,'NUMBER OF MODES PER ELEMENT = ',$)
 730  FORMAT(3X,'NUMBER OF ELEMENTS = ',$)

 735  FORMAT(3X,'DO ALL DIRECTORS HAVE THE SAME LENGTH? ',/,3X
     &      ,'ANSWER: (Y OR N) in single quotes ...... ',$)
 736  FORMAT(/,3X,'THE UNIFORM LENGTH (in WLS) OF THE DIRECTORS = ',$)
 737  FORMAT(3X,'IS THE SEPARATION BETWEEN DIRECTORS UNIFORM? ',/,3X
     &      ,'ANSWER: (Y OR N) in single quotes ...... ',$)
 738  FORMAT(/,3X,'THE UNIFORM SEPARATION (in WLS) BETWEEN DIRECTORS = '
     &      ,$)

 740  FORMAT(3X,'LENGTH (in WLS) OF DIRECTOR # ',1X,I2,' = ',$
     &      )
 742  FORMAT(3X,'LENGTH (in WLS) OF REFLECTOR = ',$)
 744  FORMAT(3X,'LENGTH (in WLS) OF DRIVEN ELEMENT = ',$)
 750  FORMAT(3X,'SEPARATION (in WLS) BETWEEN DIRECTORS #',
     &      1X,I2,' AND #',1X,I2,' = ',$)
 752  FORMAT(3X,'SEPARATION (in WLS) BETWEEN REFLECTOR & DRIVEN ELM. = '
     &      ,$)
 754  FORMAT(/,3X
     &      ,'SEPARATION (in WLS) BETWEEN DRIVEN ELM. & 1ST DIR. = ',$)
 760  FORMAT(/,3X,'RADIUS (in WLS) FOR ALL ELEMENTS USED = ',$)
 770  FORMAT(/,3X,'WAIT ... This might take a few minutes !!!')

 800  FORMAT(/,3X
     &      ,'******************************************************',/
     &      ,3X,'PROGRAM INPUT FOR THE YAGI UDA ARRAY',/,3X
     &      ,'******************************************************',/)

 820  FORMAT(/,3X,'NUMBER OF MODES PER ELEMENT = ',I2)
 830  FORMAT(3X,'NUMBER OF ELEMENTS = ',I2,/)
 840  FORMAT(3X,'LENGTH OF DIRECTOR # ',1X,I2,' = ',F12.5,' WLS')
 842  FORMAT(3X,'LENGTH OF REFLECTOR = ',F12.5,' WLS')
 844  FORMAT(3X,'LENGTH OF DRIVEN ELEMENT = ',F12.5,' WLS')
 850  FORMAT(3X,'SEPARATION BETWEEN DIRECTORS #',1X,I2,' AND #',1X,I2
     &      ,' = ',F12.5,' WLS')
 852  FORMAT(3X,'SEPARATION BETWEEN REFLECTOR & DRIVEN ELM. = ',F12.5
     &      ,' WLS')
 854  FORMAT(/,3X,'SEPARATION BETWEEN DRIVEN ELM. & 1ST DIR. = ',F12.5
     &      ,' WLS')
 860  FORMAT(/,3X,'RADIUS FOR ALL ELEMENTS USED = ',E12.5,' WLS')
C
      STOP  ! END OF THE MAIN PROGRAM  

C     ********** ERROR CONDITIONS **********
C
 999     WRITE(6,1000)
 1000    FORMAT(/,3X,'***** ERROR *****',/,3X
     $         ,'INPUT DATA ARE NOT OF THE RIGHT FORMAT',/)

      END
C****************************************************************
C
C
      SUBROUTINE KERNEL(G2,ZP)
      COMPLEX G2,CJ
      REAL PI,K,Z,ZP
      REAL RMINUS,RPLUS,RHO
      COMMON/EXTRAS/Z,RHO,N2,NMODE
C     
      CJ=CMPLX(0.0,1.0)
      PI=4.0*ATAN(1.0)
      K=2.0*PI
      RMINUS=SQRT(RHO*RHO+(Z-ZP)*(Z-ZP))
      RPLUS=SQRT(RHO*RHO+(Z+ZP)*(Z+ZP))
      G2=CEXP(-CJ*K*RMINUS)/(4.0*PI*RMINUS)
     &      +CEXP(-CJ*K*RPLUS)/(4.0*PI*RPLUS)
      RETURN
      END
C*****************************************************************
C
C
      FUNCTION ZMINUS(TH,LG,NM)
      REAL TH,LG,ARG1,PI,K,ZMINUS,F2M
      INTEGER NM
      PI=4.0*ATAN(1.0)
      K=2.0*PI
      F2M=FLOAT(2*NM-1)
      ARG1=(F2M*PI/LG-K*COS(TH))*(LG/2.0)
      IF(ARG1.EQ.0.0)THEN
         ZMINUS=1.0
      ELSE
         ZMINUS=SIN(ARG1)/ARG1
      ENDIF
      RETURN
      END
C*****************************************************************
C
C
      FUNCTION ZPLUS(TH,LG,NM)
      REAL TH,LG,ARG2,PI,K,ZPLUS,F2M
      INTEGER NM
      PI=4.0*ATAN(1.0)
      K=2.0*PI
      F2M=FLOAT(2*NM-1)
      ARG2=(F2M*PI/LG+K*COS(TH))*(LG/2.0)
      IF(ARG2.EQ.0.0)THEN
         ZPLUS=1.0
      ELSE
         ZPLUS=SIN(ARG2)/ARG2
      ENDIF
      RETURN
      END
C*****************************************************************
C
C
      FUNCTION FF(X,L)
      COMPLEX FF,CJ
      REAL X,PI,K,Z,F2M,L(*)
      REAL RMINUS,RPLUS,RHO
      COMMON/EXTRAS/Z,RHO,N2,NMODE
C
      CJ=CMPLX(0.0,1.0)
      PI=4.0*ATAN(1.0)
      K=2.0*PI
      RMINUS=SQRT(RHO*RHO+(Z-X)*(Z-X))
      RPLUS=SQRT(RHO*RHO+(Z+X)*(Z+X))
      F2M=FLOAT(2*NMODE-1)
      FF=(CEXP(-CJ*K*RMINUS)/(4.0*PI*RMINUS)
     &      +CEXP(-CJ*K*RPLUS)/(4.0*PI*RPLUS))*
     &      COS(F2M*PI*X/L(N2))
      RETURN
      END
C*****************************************************************
C
C
      FUNCTION FXZ(THETA,PHI,N,M,Inm,L,YP,NMAX,MMAX)
      EXTERNAL ZMINUS,ZPLUS
      COMPLEX FXZ,AZ,CJ,IZP,Inm(NMAX,MMAX)
      REAL THETA,PHI,ZMINUS,ZPLUS,U
      REAL PI,LEN,AEXP,K,L(*),YP(*)
      CJ=CMPLX(0.0,1.0)
      PI=4.0*ATAN(1.0)
      K=2.0*PI
      AZ=CMPLX(0.0,0.0)
      DO 10 I=1,N
         IZP=CMPLX(0.0,0.0)
         DO 20 J=1,M
            MODE=J
            LEN=L(I)
            ANG=THETA
            IZP=IZP+Inm(I,J)*(ZMINUS(ANG,LEN,MODE)+ZPLUS(ANG,LEN,MODE))
 20      CONTINUE
         AEXP=K*YP(I)*SIN(THETA)*SIN(PHI)
         AZ=AZ+L(I)*CEXP(CJ*AEXP)*IZP
 10   CONTINUE
      U=3.75*PI*CABS(AZ)*CABS(AZ)*SIN(THETA)*SIN(THETA)
      FXZ=SIN(THETA)*U*CMPLX(1.0,0.0)
      RETURN
      END
C***************************************************************** 
C     SUBROUTINE SINTEG  (SINGLE PRECISION)
C
C     PURPOSE
C     TO PERFORM COMPLEX SINGLE INTEGRATION
C     DOES SIXTEEN POINT GAUSSIAN QUADRATURE INTEGRATION
C     WITH INCREASING ACCURACY SET BY INTEGER NO
C
C     USAGE
C     CALL SINTEG(FX,UL,LL,NO,ANS,L)
C
C     DESCRIPTION OF PARAMETERS
C     FX  -  COMPLEX FUNCTION OF A SINGLE REAL VARIABLE
C     UL  -  UPPER LIMIT OF THE INTEGRATION (REAL)
C     LL  -  LOWER LIMIT OF THE INTEGRATION (REAL)
C     NO  -  NUMBER OF DIVISIONS BETWEEN LL AND UL  (INTEGER)
C     ANS -  RESULT OF INTEGRATION
C     L   - THE LENGTHS OF THE ARRAY ELEMENTS
C
      SUBROUTINE SINTEG(FX,UL,LL,NO,ANS,L)
      REAL UL,LL,X,GAUSS(16),LEGEND(16),DEL,S,L(*)
      COMPLEX FX,SUM,ANS
      DATA  GAUSS/-0.0950125098376370,-0.2816035507792590,
     &            -0.4580167776572270,-0.6178762444026440,
     &            -0.7554044083550030,-0.8656312023878320,
     &            -0.9445750230732330,-0.9894009349916500,
     &             0.0950125098376370,0.2816035507792590,
     &             0.4580167776572270,0.6178762444026440,
     &             0.7554044083550030,0.8656312023878320,
     &             0.9445750230732330,0.9894009349916500/
      DATA  LEGEND/0.1894506104550680,0.1826034150449240,
     &             0.1691565193950020,0.1495959888165770,
     &             0.1246289712555340,0.0951585116824930,
     &             0.0622535239386480,0.0271524594117540,
     &             0.1894506104550680,0.1826034150449240,
     &             0.1691565193950020,0.1495959888165770,
     &             0.1246289712555340,0.0951585116824930,
     &             0.0622535239386480,0.0271524594117540/
      DEL=(UL-LL)/FLOAT(2*NO)
      SUM=(0.0,0.0)
      DO 1 J=1,NO
         S=LL+FLOAT(2*J-1)*DEL
         DO 2 I=1,16
            X=S+GAUSS(I)*DEL
            SUM=SUM+LEGEND(I)*FX(X,L)
 2       CONTINUE
 1    CONTINUE
      ANS=SUM*DEL
      RETURN
      END
C******************************************************************
C SUBROUTINE SCINT2
C SCINT2 IS A SINGLE PRECISION, COMPLEX, INTEGRATION ROUTINE
C IN 2 DIMENSIONS.  THIS ROUTINE USES 16 POINT GAUSSIAN QUADRATURES,
C WITH LEGANDRE COEFFICIENTS.  ENTER WITH:
C
C       (C)FXZ         FUNCTION OF INTEGRAND
C       (R)X1          LOWER LIMIT OF X INTEGRATION
C       (R)X2          UPPER LIMIT OF X INTEGRATION
C       (R)Z1          LOWER LIMIT OF Z INTEGRATION
C       (R)Z2          UPPER LIMIT OF Z INTEGRATION
C       (C)RES         RESULTS OF INTEGRATION
C       (I)N         NUMBER OF ELEMENTS IN THE ARRAY
C       (I)M          NUMBER OF MODES PER ELEMENT
C       (R)Inm          THE ARRAY OF CURRENT COEFFICIENTS
C       (R)L           THE ELEMENT LENGTHS
C       (R)YP          THE ELEMENT DISTANCES ALONG THE Y AXIS
C
C***********************************************************************
C
C
      SUBROUTINE SCINT2(FXZZ,X1,X2,Z1,Z2,RES,N,M,Inm,L,YP,NMAX,MMAX)
      COMPLEX    FXZZ,RES,S,SS,TT,Inm(NMAX,MMAX)
      REAL       R(16),W(16),L(*),YP(*)

      DATA R/ 0.0950125098, 0.2816035508, 0.4580167777,
     &        0.6178762444, 0.7554044084, 0.8656312024,
     &        0.9445750231, 0.9894009350,-0.9894009350,
     &       -0.9445750231,-0.8656312024,-0.7554044084,
     &       -0.6178762444,-0.4580167777,-0.2816035508,
     &       -0.0950125098 /
 
      DATA W/ 0.1894506105, 0.1826034150, 0.1691565194,
     &        0.1495959888, 0.1246289713, 0.0951585117,
     &        0.0622535239, 0.0271524594, 0.0271524594,
     &        0.0622535239, 0.0951585117, 0.1246289713,
     &        0.1495959888, 0.1691565194, 0.1826034150,
     &        0.1894506105 /
      SX = 0.5*(X2+X1)
      DX = 0.5*(X2-X1)
      SZ = 0.5*(Z2+Z1)
      DZ = 0.5*(Z2-Z1)
      TT = CMPLX(0.0,0.0)
      DO 20 J=1,16
         Z  = SZ+DZ*R(J)
         SS = CMPLX(0.0,0.0)
         DO 30 I=1, 16
            X  = SX+DX*R(I)
            SS = W(I)*FXZZ(X,Z,N,M,Inm,L,YP,NMAX,MMAX)+SS
 30      CONTINUE
         S = DX*SS
         TT = S*W(J)+TT
 20   CONTINUE
      RES = DZ*TT
      RETURN
      END
C
      FUNCTION PHASE(CCCC)
      REAL PHASE,XC(2)
      COMPLEX CX,CCCC
C
      EQUIVALENCE(CX,XC(1))
      CX=CCCC
      PHASE=ATAN2(XC(2),XC(1))
C
      RETURN
      END


C     *****************************************************************  
      SUBROUTINE LUDEC (Z,N,NP,IPERM,SCAL)
C     *****************************************************************

      INTEGER N,NP
      INTEGER IPERM(*)
      REAL    SCAL(*)
      COMPLEX Z(NP,NP)

C    REPLACES MATRIX BY ITS LU DECOMPOSITION

      INTEGER I,J,K,IMAX
      REAL    ZMAX,CAZ,TEST
      COMPLEX TEMP

C    GET SCALING INFO.
      DO 20 I=1,N
         ZMAX=0.E0
         DO 10 J=1,N
            CAZ=CABS(Z(I,J))
            IF (CAZ.GT.ZMAX) ZMAX=CAZ
10       CONTINUE
         SCAL(I)=1.E0/ZMAX
20    CONTINUE

C    CROUT's algorithm.
      DO 80 J=1,N
         DO 30 I=1,J-1
            DO 30 K=1,I-1
            Z(I,J)=Z(I,J)-Z(I,K)*Z(K,J)
30       CONTINUE
         ZMAX=0.E0
C    SEARCH FOR LARGEST PIVOT ELEMENT.
         DO 50 I=J,N
            DO 40 K=1,J-1
               Z(I,J)=Z(I,J)-Z(I,K)*Z(K,J)
40          CONTINUE
            TEST=SCAL(I)*CABS(Z(I,J))
            IF (TEST.GE.ZMAX) THEN
               IMAX=I
               ZMAX=TEST
            END IF
50       CONTINUE

C    INTERCHANGE THE ROWS.
         IF(J.NE.IMAX)THEN
            DO 60 K=1,N
               TEMP=Z(IMAX,K)
               Z(IMAX,K)=Z(J,K)
               Z(J,K)=TEMP
60          CONTINUE
            SCAL(IMAX)=SCAL(J)
         END IF
         IPERM(J)=IMAX
C    DIVIDE BY PIVOT ELEMENT.
         IF(J.NE.N)THEN
            DO 70 I=J+1,N
               Z(I,J)=Z(I,J)/Z(J,J)
70          CONTINUE
         END IF
80    CONTINUE

      RETURN
      END

C     *****************************************************************
      SUBROUTINE LUSOLV(Z,N,NP,IPERM,V)
C     *****************************************************************

      IMPLICIT NONE

      INTEGER N,NP
      INTEGER IPERM(*)
      COMPLEX Z(NP,NP),V(*)

C    SOLVES LINEAR SYSTEM GIVEN THE LU DECOMPOSITION FROM LUDEC
C    FORCING VECTOR IS REPLACED WITH SOLUTION VECTOR UPON EXIT 

      INTEGER I,J,II
      COMPLEX TEMP

C    FORWARD SUBSTITUTION.
      DO 20 I=1,N
         TEMP=V(IPERM(I))
         V(IPERM(I))=V(I)
         DO 10 J=1,I-1
            TEMP=TEMP-Z(I,J)*V(J)
10       CONTINUE
         V(I)=TEMP
20    CONTINUE

C    BACKWARD SUBSTITUTION.
      DO 40 I=1,N
         II=N-I+1
         TEMP=V(II)
         DO 30 J=II+1,N
            TEMP=TEMP-Z(II,J)*V(J)
30       CONTINUE
         V(II)=TEMP/Z(II,II)
40    CONTINUE

      RETURN
      END





